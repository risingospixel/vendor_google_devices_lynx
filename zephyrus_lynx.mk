# Generated by adevtool; do not edit
# For more info, see https://github.com/kdrag0n/adevtool

# Inherit AOSP product
$(call inherit-product, device/google/lynx/aosp_lynx.mk)

# Match stock product info
PRODUCT_NAME := zephyrus_lynx
PRODUCT_MODEL := Pixel 7a
PRODUCT_BRAND := google
PRODUCT_MANUFACTURER := Google
